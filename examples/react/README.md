# react
rollup-plugin-static-site example for [react](https://reactjs.org)

## notes
- `yarn build` creates a build in `dist` for production
  - `serve -s dist` can be used to view locally (requires [serve](https://github.com/zeit/serve))
- `yarn start` watches source files and starts browsersync for development
- adapted from [create-react-app](https://github.com/facebook/create-react-app)'s template
  - uses [css modules](https://github.com/css-modules/css-modules)
  - no tests 🙃
- react and react-dom via unpkg
