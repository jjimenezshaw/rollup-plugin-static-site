# rollup-plugin-static-site
generate html out of thin air (or with any templating engine) for your static site bundle

[![npm version](https://img.shields.io/npm/v/rollup-plugin-static-site.svg?style=popout)](https://www.npmjs.com/package/rollup-plugin-static-site)
[![npm downloads](https://img.shields.io/npm/dw/rollup-plugin-static-site.svg?style=popout)](https://www.npmjs.com/package/rollup-plugin-static-site)

## install
```sh
yarn add -D rollup-plugin-static-site
```

## usage
```javascript
// rollup.config.js
import staticSite from 'rollup-plugin-static-site';

export default {
  input: 'src/index.js',
  output: {
    file: 'dest/js/bundle.js',
    format: 'iife',
  },
  plugins: [
    staticSite({ dir: 'dest' }),
  ]
};
```
running `rollup -c` with the above config will create static files in `dest`.
the output html will be in `dest/index.html` and will have a script tag pointing to the rollup bundle.

## options
opts <code>Object</code> - plugin options
  - .dir <code>string</code> - path to output directory containing assets and bundle
  - [.css] <code>boolean</code> | <code>string</code> <code> = false</code> - path to css file.
      typically the value of rollup-plugin-postcss' `extract` option.
  - [.filename] <code>string</code> <code> = &quot;index.html&quot;</code> - filename of the output html
  - [.moreScripts] <code>Array.&lt;string&gt;</code> | <code>string</code> <code> = []</code> - additional scripts that should be injected
      into the output html, useful for loading libraries via a cdn instead the bundle
  - [.moreStyles] <code>Array.&lt;string&gt;</code> | <code>string</code> <code> = []</code> - like `opts.moreScripts`, but for css
  - [.template] <code>Object</code> <code> = {}</code> - custom template options
    - .func <code>function</code> - wrapper function used for custom templating engines.
        has signature `(templateStr, templateData) => finalHtml`,
        where `templateStr` is the contents of the custom template (`opts.template.path`)
        and `templateData` is the result of merging `opts.title` and `opts.template.data`
        with two array properties, `scripts` and `styles`.
        `scripts` is `opts.moreScripts` with the path to the bundle `opts.dir` appended.
        `styles` is `opts.moreStyles` with `opts.css` appended, if given.
        this function should call whatever custom templating engine api necessary with the arguments
        in order to return `finalHtml`, a string of html that the plugin will save.
    - .path <code>string</code> - path to custom template.
        if `func` is not given, the default doT engine will be used.
        the plugin will inject template strings to handle `scripts` and `styles` data if necessary.
    - [.data] <code>Object</code> <code> = {}</code> - template data object.
        `scripts` and `styles` are reserved and will be overwritten if present.
  - [.title] <code>string</code> <code> = &quot;rollup app&quot;</code> - string used for the `<title>` tag in the output html

## test
```sh
yarn test # or yarn test:cov
```

## motivation
i recently got back into making web-based creative coding sketches.
for me, this means static html with some client-side js.
instead of going with the familiar [webpack](https://github.com/webpack/webpack) + [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin),
i wanted to try something new, so i went ahead and installed [rollup](https://github.com/rollup/rollup).
the rollup wiki led me to [rollup-plugin-generate-html-template](https://github.com/bengsfort/rollup-plugin-generate-html-template),
which works well for very simple projects, but quickly becomes unusable due to inflexibility.
i tried some other html-related plugins with no luck,
so i wrote one myself 😈.
