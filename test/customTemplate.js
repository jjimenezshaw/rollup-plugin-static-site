import { relative } from 'path';

import test from 'ava';
import fs from 'fs-extra';

import {
  afterEach,
  beforeEach,
  build,
  attrRegex,
  linkRegex,
  scriptRegex,
  templatePaths,
} from './_helpers';

test.beforeEach(beforeEach);

test.afterEach(afterEach);

test('write html based on custom template when `opts.template.path` is a path', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const path = templatePaths.dot;

  const bundle = await build({
    dir,
    template: {
      path,
    },
  });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  t.regex(output, /<div id="hero"><\/div>/);
});

test('error out when `opts.template.path` is not a valid path', async t => {
  t.plan(1);
  const {
    dir,
    format,
    js,
  } = t.context;
  const path = '/does/not/exist';

  const bundle = await build({
    dir,
    template: {
      path,
    },
  });
  await t.throws(bundle.write({ format, file: js }), /ENOENT/);
});

test('write html based on custom template with injected script tag', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const path = templatePaths.dot;

  const bundle = await build({
    dir,
    template: {
      path,
    },
  });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  t.regex(output, scriptRegex(scriptPath));
});

test('write html based on custom template with injected link tag', async t => {
  t.plan(1);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const path = templatePaths.dot;

  const bundle = await build({
    css,
    dir,
    template: {
      path,
    },
  });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  t.regex(output, linkRegex(linkPath));
});

test('write html without modifying custom template when it handles `scripts` data', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const path = templatePaths.dotCustomArrays;

  const bundle = await build({
    dir,
    template: {
      path,
    },
  });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  t.regex(output, /<script async nomodule/);
});

test('write html without modifying custom template when it handles `styles` data', async t => {
  t.plan(1);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const path = templatePaths.dotCustomArrays;

  const bundle = await build({
    css,
    dir,
    template: {
      path,
    },
  });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  t.regex(output, new RegExp(`<link${attrRegex}type="text/css">`));
});
