# changelog

## [unreleased 1.0.0]
### added
- gitlab ci
- mit license
- official examples
  - bootstrap
  - mustache
  - p5
  - pts
  - pug
  - react
- readme badges

## [0.0.3] - 2018-08-19
### fixed
- readme indentation

## [0.0.2] - 2018-08-19
### added
- readme usage
- husky for hooks
- lint-staged for linting js

### changed
- readme options from table to list

## [0.0.1] - 2018-08-19
### added
- plugin code
- unit tests
